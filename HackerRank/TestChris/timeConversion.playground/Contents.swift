import UIKit

func timeConversion(s: String) -> String {
    let arr = s.split(separator: ":")
    var result: String = ""
    var resultInt: Int = 0
    if let hourInt = Int(arr[0]) {
        if s.contains("AM") {
            resultInt = (hourInt) % 12
        } else {
            if hourInt == 12 {
                resultInt = hourInt
            } else {
                resultInt = (hourInt + 12) % 24
            }
        }
        if resultInt < 10 {
            result = "0\(resultInt):\(arr[1]):\(arr[2])"
        } else {
            result = "\(resultInt):\(arr[1]):\(arr[2])"
        }
        result = result.replacingOccurrences(of: "AM", with: "")
        result = result.replacingOccurrences(of: "PM", with: "")
    }
    return result
}
