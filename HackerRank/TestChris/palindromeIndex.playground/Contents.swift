import UIKit

func palindromeIndex(s: String) -> Int {
    let count = s.count
    var asciiArray: [Int] = []
    
    if count >= 0 && count < 3 {
      return -1
    }

    for item in s {
      if let ascciValue = item.asciiValue {
        asciiArray.append(Int(ascciValue))
      }
    }
    
    return calculateAsciiArray(asciiArray: asciiArray, count: count)
}

func calculateAsciiArray(asciiArray: [Int], count: Int) -> Int {
  var index = 0
  for _ in asciiArray {
    if asciiArray[index] != asciiArray[count - 1 - index] {
      var aux = asciiArray
      aux.remove(at: count - 1 - index)
      if comprobarArray(arr: aux)  {
        return count - 1 - index
      }
      return index
    }
    index += 1
  }
  return -1
}

func comprobarArray(arr: [Int]) -> Bool {
  let count = arr.count
  var index = 0
  for _ in arr {
    if arr[index] != arr[count - 1 - index] {
      return false
    }
    index += 1
  }
  return true
}
