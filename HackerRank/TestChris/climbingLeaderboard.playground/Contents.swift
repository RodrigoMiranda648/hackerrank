import UIKit

func climbingLeaderboard(ranked: [Int], player: [Int]) -> [Int] {
    var results: [Int] = []
    var savedResults: [Int: Int] = [:]
    var sortedArray = sortArray(arr: ranked)
    for newScore in player {
        if let index = savedResults[newScore] {
            results.append(index)
        } else {
            let position = getRank(arr: sortedArray, newScore: newScore)
            savedResults[newScore] = position
            results.append(position)
        }
    }
    return results
}

func sortArray(arr: [Int]) -> [Int] {
    let newSet = Set(arr)
    var newArray = Array(newSet)
    newArray.sort(by: {$0 > $1 })
    return newArray
}

func getRank(arr: [Int], newScore: Int) -> Int {
    if let maxScore = arr.first, newScore >= maxScore  {
        return 1
    }
   
    if let minScore = arr.last {
        if newScore == minScore {
            return arr.count
        } else if newScore < minScore {
            return arr.count + 1
        }
    }
    
    var minPossition = 0
    var maxPosition = arr.count - 1
    var middlePosition = (maxPosition + minPossition) / 2
    while newScore > arr[maxPosition] {
        if arr[middlePosition] < newScore {
            maxPosition = middlePosition - 1
        } else if arr[middlePosition] > newScore {
            minPossition = middlePosition + 1
        } else {
            return middlePosition + 1
        }
        
        if arr[maxPosition] > newScore {
            return maxPosition + 2
        } else if arr[maxPosition] == newScore {
            return maxPosition + 1
        } else {
            middlePosition = (maxPosition + minPossition) / 2
        }
    }
    return maxPosition + 2
}

print(climbingLeaderboard(ranked: [100,100,50,40,40,20,10], player: [5,25,50,120]))
