import UIKit

func getBufferedAddress<T>(array: [T]) -> String{
    return array.withUnsafeBufferPointer { buffer in
        return String(reflecting: buffer.baseAddress)
    }
}
