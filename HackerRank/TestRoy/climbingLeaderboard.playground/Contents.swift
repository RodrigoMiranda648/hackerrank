import UIKit

func climbingLeaderboard(ranked: [Int], player: [Int]) -> [Int] {
    var playerRankings = [Int]()
    let rankedNoDuplicates = Array(Set(ranked)).sorted(by: >)
    
    for score in player {
        let rank = findRank(score: score, rankings: rankedNoDuplicates)
        playerRankings.append(rank)
    }
    
    return playerRankings
}

func findRank(score: Int, rankings: [Int]) -> Int {
    var low = 0
    var high = rankings.count - 1
    
    if rankings[low] <= score {
        return low + 1
    }
    if rankings[high] == score {
        return high + 1
    }
    if rankings[high] > score {
        return high + 2
    }
    
    while low <= high {
        let mid = (low + high) / 2
        
        if rankings[mid] == score {
            return mid + 1
        } else if rankings[mid] < score {
            high = mid - 1
        } else {
            low = mid + 1
        }
    }
    return low + 1
}

print(climbingLeaderboard(ranked: [100,100,50,40,40,20,10], player: [5,25,50,120]))
