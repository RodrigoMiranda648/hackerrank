import UIKit

let regularExpression = #"^0*1{1}0*$"#

func esPotenciaDeDos(_ binario: String) -> Bool {
    return binario.range(of: #"^0*10*$"#, options: .regularExpression) != nil
}
esPotenciaDeDos("0100")
