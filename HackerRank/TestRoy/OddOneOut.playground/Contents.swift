import UIKit

func findOdd(_ series: [String]) -> String {
    var distances = [[Int]]()
    var oddOneOut = ""

    for s in series {
        var dist = [Int]()
        for i in 0..<(s.count - 1) {
            let char1 = s[s.index(s.startIndex, offsetBy: i)]
            let char2 = s[s.index(s.startIndex, offsetBy: i + 1)]
            dist.append(Int(char2.asciiValue!) - Int(char1.asciiValue!))
        }
        distances.append(dist)
    }

    for i in 0..<distances.count {
        if distances.filter({ $0 == distances[i] }).count == 1 {
            oddOneOut = series[i]
            break
        }
    }

    return oddOneOut
}

let series = ["abcde", "fghijkl", "mnopqrst", "uvwxy", "zab"]
let oddOneOut = findOdd(series)
print("El elemento fuera de lugar es: \(oddOneOut)")
