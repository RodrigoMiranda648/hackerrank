import UIKit

let arr:[Int] = [1,1,1,2,3]
//The numbers could be from zero to 99 that's why the array is of 100 elements
func countingSort(arr: [Int]) -> [Int] {
    var result = Array(repeating: 0, count: 100)
    for element in arr{
        result[element] += 1
    }
    return result
}

print(countingSort(arr: arr))
