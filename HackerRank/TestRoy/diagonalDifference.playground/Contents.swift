import UIKit
//Get the absolute difference between both diagonals of a matrix

var matrix: [[Int]] = []
matrix.append([1, 2, 3])
matrix.append([4, 5, 6])
matrix.append([7, 8, 9])

func diagonalDifference(arr: [[Int]]) -> Int {
    // Write your code here
    var mainDiagonal = 0
    var inverseDiagonal = 0
    let count = matrix.count
    for i in 0 ..< count{
        for j in 0 ..< matrix[i].count{
            if i == j {
                mainDiagonal += matrix[i][j]
            }
            if count-1 - i == j{
                inverseDiagonal += matrix[i][j]
            }
        }
    }
    return abs(mainDiagonal - inverseDiagonal)
}

diagonalDifference(arr: matrix)
