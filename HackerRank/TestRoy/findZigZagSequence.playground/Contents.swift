import UIKit

func findZigZagSequence(_ a: [Int]) {
    var a = a
    a.sort()
    let n = a.count
    
    let mid = n / 2
    a.swapAt(mid, n - 1)

    var st = mid + 1
    var ed = n - 2
    var i = 0
    while st <= ed {
        i += 1
        a.swapAt(st, ed)
        st += 1
        ed -= 1
    }

    for i in 0..<n {
        if i == n - 1 {
            print(a[i])
        } else {
            print(a[i], terminator: " ")
        }
    }
}

findZigZagSequence([1,2,3,4,5,6,7])
