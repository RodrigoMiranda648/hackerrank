import UIKit

let pal = "abbaaabaa"

func palindromeIndex(s: String) -> Int {
    let count = s.count
    var stringArray = s.map { String($0) }
    
    if count >= 0 && count < 3 {
        return -1
    }
    return calculateAsciiArray(array: stringArray, count: count)
}

func calculateAsciiArray(array: [String], count: Int) -> Int {
  var index = 0
  for _ in array {
    if array[index] != array[count - 1 - index] {
      var aux = array
      aux.remove(at: count - 1 - index)
      if comprobarArray(arr: aux)  {
        return count - 1 - index
      }
      return index
    }
    index += 1
  }
  return -1
}

func comprobarArray(arr: [String]) -> Bool {
  let count = arr.count
  var index = 0
  for _ in arr {
    if arr[index] != arr[count - 1 - index] {
      return false
    }
    index += 1
  }
  return true
}

palindromeIndex(s: pal)
