import UIKit

func dayOfProgrammer(year: Int) -> String {
    // Write your code here
    let februaryDays = getFebruaryDays(year: year)
    let daysInSeptember = getRemainingDays(daysOfFebruary: februaryDays)
    return "\(daysInSeptember).09.\(year)"
}
func getRemainingDays(daysOfFebruary: Int)->Int{
    //Add days from January to august
    let daysToSeptember = 31 + daysOfFebruary + 31 + 30 + 31 + 30 + 31 + 31
    return 256 - daysToSeptember
}

func getFebruaryDays(year: Int) -> Int{
    let isDivisibleBy400: Bool = (year % 400 == 0)
    let isDivisibleBy4: Bool = (year % 4 == 0)
    let isNotDivisibleBy100: Bool = (year % 100 != 0)
    
    if year == 1918{
        return 28 - 13
    }
    
    if year < 1918 {
        if isDivisibleBy4{
            return 29
        }else{
            return 28
        }
    }
    
    if isDivisibleBy400 || (isDivisibleBy4 && isNotDivisibleBy100){
        return 29
    }else{
        return 28
    }
}

print(dayOfProgrammer(year: 1800))
