import UIKit

func caesarCipher(s: String, k: Int) -> String {
    // Write your code here
    var alpha = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    var cipher = k > 0 ? getCipher(abc: alpha, k: k) : alpha
    print(cipher)
    var message: String = ""
    for character in s{
        if let index = alpha.firstIndex(where: { $0 == String(character).uppercased() }) {
            message += character.isUppercase ? cipher[index] : cipher[index].lowercased()
            continue
        }
        message += String(character)
    }
    return message
}
func getCipher(abc: [String], k: Int) -> [String]{
    var result: [String] = abc
    for i in 1 ... k{
        let first = result.remove(at: 0)
        result += [first]
    }
    return result
}

caesarCipher(s: "D3q4", k: 0)
