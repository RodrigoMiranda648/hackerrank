import UIKit

let series = ["ADC", "BED", "CFE", "DGF", "FGH"]
//[123234567432]
var isError: Bool = false
var index: Int = 0
let alphabeth: [String: Int] = ["A": 1, "B": 2, "C":3,"D":4,"E":5,"F":6,"G":7,"H":8,"I":9,"J":10,"K":11,"L":12,"M":13,"N":14,"O":15,"P":16,"Q":17,"R":18,"S":19,"T":20,"U":21,"V":22,"W":23,"X":24,"Y":25,"Z":26]
var results: [(Int, Int)] = []
var numbers: [[Int]] = []

func findOdd(series: [String]) -> String {
    start(stringArray: series)
    let position = getResult()
    if position == -1{
        return ""
    }
    return series[position]
}

func start(stringArray: [String]) {
    for serie in stringArray {
        if serie.count == 3 {
            convertStringToInt(serie: serie)
        }
    }
}

func convertStringToInt(serie: String) {
    var arrayAux: [Int] = []
    for caracter in serie {
        arrayAux.append(alphabeth["\(caracter)"] ?? 0)
    }
    numbers.append(arrayAux)
}

func getResult() -> Int {
    for serieOfNumbers in numbers {
        makeOperations(arr: serieOfNumbers)
    }
    return getPosition()
}

func makeOperations(arr: [Int]) {
    if arr.count==3{
        let anterior = arr[0]
        let pivote = arr[1]
        let siguiente = arr[2]
        let tupla = (pivote - anterior, pivote - siguiente)
        results.append(tupla)
    }
    else{
        index = arr[0]
        isError = true
    }
}

func getPosition() -> Int {
    if !results.isEmpty{
        let pivote = results[0]
        var i = 0
        var position = 0
        for item in results {
            if item != pivote {
                position = i
            }
            i += 1
        }
        return position
    }
    return -1
}

findOdd(series: series)
